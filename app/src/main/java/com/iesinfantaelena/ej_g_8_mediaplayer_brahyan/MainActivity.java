package com.iesinfantaelena.ej_g_8_mediaplayer_brahyan;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnGato, btnLeon;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGato = findViewById(R.id.btnGato);
        btnLeon = findViewById(R.id.btnLeon);

        btnGato.setOnClickListener(this);
        btnLeon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view == btnGato){
            if(mediaPlayer != null){
                mediaPlayer.release();
                mediaPlayer = null;
            }
            mediaPlayer = MediaPlayer.create(this,R.raw.gato);
            mediaPlayer.start();
        }

        if( view == btnLeon ){
            if(mediaPlayer != null){
                mediaPlayer.release();
                mediaPlayer = null;
            }
            mediaPlayer = MediaPlayer.create(this,R.raw.leon);
            mediaPlayer.start();
        }
    }
}